#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

char upcase(char ch){
  if(islower(ch))
    ch -= 'a' - 'A';
  return ch;
}

char* fixkey(char* s){
  int i, j,k;
  char plain[26]; // assume key < length of alphabet, local array on stack, will go away!

  for(i = 0, j = 0; i < strlen(s); i++){
    if(isalpha(s[i])){
      plain[j++] = upcase(s[i]);
    }
  }
  plain[j] = '\0'; 
  int n = strlen(plain);
  for(i=0; i<n;i++){
    for(j=i+1;j<n;){
        if(plain[j]==plain[i]){
            for(k=j; k<n;k++){
                plain[k]=plain[k+1];}
            n--;}
        else{j++;}
    }
  }

  return strcpy(s, plain);
}

void buildtable (char* key, char* decode){

  int plainalpha[26] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
  char alpha[26] = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N',
                        'O','P','Q','R','S','T','U','V','W','X','Y','Z'};

  char* encode = (char*)malloc(sizeof(char)*26);
    
  int cipherlength = strlen(key)-1;
  fixkey(key);
  int keylength = strlen(key);
  int i,j;

  for(i=0;i<keylength;i++){
    j = key[i]-65; 
    encode[cipherlength+i] = key[i];
    plainalpha[j] = 0;   
  }
    
   i=cipherlength+keylength;
   while((i%26)!=cipherlength){
   j++;
   if(plainalpha[j%26]==1){
       encode[i%26] = alpha[j%26];
       i++;}
   }

   for(i=0;i<26;i++){
    for(j=0;j<26;j++){
        if(alpha[i]==encode[j]){
           decode[i]=alpha[j];}
    }
   }
     
 }      


int main(int argc, char **argv){

  char* decode = (char*)malloc(sizeof(char)*26);
  char ch;

  if(argc != 2){
    printf("format is: '%s' key", argv[0]);
    exit(1);
  }    
    
  buildtable(argv[1], decode);
  fprintf(stderr,"key: %s - %d\n", decode, strlen(decode));
  
  ch = fgetc(stdin);
  while (!feof(stdin)) {
    if(isalpha(ch)){        // only encrypt alpha chars
      ch = upcase(ch);      // make it uppercase
      fputc(decode[ch-'A'], stdout);
    }else 
      fputc(ch, stdout);
    ch = fgetc(stdin);      // get next char from stdin
  }

}





















