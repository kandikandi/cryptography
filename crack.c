#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define TEXT_SIZE 2000000// Note, the longer the text the more likely you will get a good 'decode' from the start.
#define ALEN 26         // Number of chars in ENGLISH alphabet
#define CHFREQ   "ETAONRISHDLFCMUGYPWBVKJXQZ" // Characters in order of appearance in English documents.
#define ALPHABET "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

//change lowercase letters to uppercase to make analysing frequencies easier
char upcase(char ch){
   if(islower(ch)){
     ch -= 'a' - 'A';
   }
   return ch;
}

//passes in an int array, then finds the maximum frequency and returns the index.
//the index is used to find the letters realtive to their index in the alphabet.
int findNextHighest(int* freq){
    int max = 0;
    int i;
    for(i=0;i<26;i++){
      if(freq[i]>freq[max]){
        max = i;}
    }
    freq[max] = -1; // change index to -1 so it will not return as the maximum again
    return max; 
}

//analyses the frequency of the letters, puts this information into an int array with the index representing
//the letters. i.e idx 0 ='A' 25 = 'Z'. Find the maximum number in this array, and assigns the associated letter 
//to another array (decrypt). decrypt associates the letters from chfreq to the relative frequencies in the freq arrays.
char* analyse(char* string){
    int freq[26] = {};
    int i = 0;

    //ignore whitespace and punctuation,analyse frequency and assign to frequency table
    while(string[i]!='\0'){
      if (string[i] >= 'A' && string[i] <= 'Z'){
         freq[string[i]-'A']++;
      }
     i++;
     }

    char decrypt[27];

    //create decryption index by find the highest each time and assigning that letter to
    //the matching frequency in CHFREQ
    for(i=0;i<26;i++){
      decrypt[findNextHighest(freq)] = CHFREQ[i]; 
    }

    decrypt[26] = '\0';
    i=0;

    //ignore whitespace and punctuation, use decrypt table to change the letters around
    while(string[i]!='\0'){
      if (string[i] >= 'A' && string[i] <= 'Z'){ 
          string[i] = decrypt[string[i]-'A'];
      } 
      i++;
    }
    return string;
}

//splits the text into n strings, then sends each string to be analysed and decrypted one by one
//then merges the decrypted strings back together and prints the result.
void crack(int n, char* text){
    int strlength = strlen(text);
    char strings [n][(strlength/n)+1];
    int i = 0,j=0,k = -1;

    //splitting the strings, including whitespace and punctuation.
    while(text[i]!='\0'){
      if(i%n==0){
        k++;
      }
      j = i%n;
      strings[j][k] = text[i];
      strings[j][k+1] = '\0';
      i++;
    }

    //analyse and decrypt each split string one by one
    for(i=0;i<n;i++){
      analyse(strings[i]);
    }
    
    //reset indexed for arrays
    k =-1;
    i =0;
    j =0;

    //merge strings back together
    while(text[i]!='\0'){
      if(i%n==0){
        k++;
      }
      j = i%n;
      text[i] =  strings[j][k];
      i++;
    }
    text[i]='\0';

    fprintf(stdout,"%s\n",text);
}


int main(int argc, char **argv){
   // first allocate some space for our input text (we will read from stdin).
   char* text = (char*)malloc(sizeof(char)*TEXT_SIZE+1);
   char ch;
   int n, i;

   if(argc > 1 && (n = atoi(argv[1])) > 0); else{ fprintf(stderr,"Malformed argument, use: crack [n], n > 0  \n");
     exit(-1);} // get the command line argument n
  
   // Now read TEXT_SIZE or feof worth of characters (whichever is smaller) and convert to uppercase as we do it.
   for(i = 0, ch = fgetc(stdin); i < TEXT_SIZE && !feof(stdin); i++, ch = fgetc(stdin)){
     text[i] = (ch = (isalpha(ch)?upcase(ch):ch));
   }
   text[i] = '\0'; // terminate the string properly.
    
   //cycle through n times, analyses each time for i alphabets
   for(i=1; i<=n; i++){
     crack(i,text);
   }
   
   return 0;
}






